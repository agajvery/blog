<?php

use AlexGaj\Blog\App;

$basePath = realpath(__DIR__ . '/../');

define('BASE_PATH', $basePath);
define('FRAMEWORK_PATH', $basePath . '/src/Blog');

require_once BASE_PATH . '/vendor/autoload.php';
$config = require_once  BASE_PATH . '/etc/app.php';

$app = new App($config);
$app->run();