<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 4:48 PM
 */

use AlexGaj\Blog\Components\Auth;
use AlexGaj\Blog\Components\BladeViewRender;
use AlexGaj\Blog\Components\DbConnectionManager;
use AlexGaj\Blog\Components\Request;
use AlexGaj\Blog\Components\Response;
use AlexGaj\Blog\Components\Session;
use AlexGaj\Blog\Controllers\MainController;
use AlexGaj\Blog\Controllers\PostController;
use AlexGaj\Blog\Controllers\UserController;
use AlexGaj\Blog\Models\Repossitory\User\MysqlUserRepository;
use AlexGaj\Blog\Models\Repossitory\User\UserRepositoryInterface;

return [
    'routes' => [
        '/' => [
            'controller' => MainController::class,
            'action' => 'indexAction'
        ],
        '/login' => [
            'controller' => UserController::class,
            'action' => 'loginAction'
        ],
        '/post/add' => [
            'controller' => PostController::class,
            'action' => 'addAction'
        ],
        '/post/view' => [
            'controller' => PostController::class,
            'action' => 'viewAction'
        ],
        '/post/me' => [
            'controller' => PostController::class,
            'action' => 'myPostsAction'
        ]
    ],
    'components' => [
        'render' => [
            'class' => BladeViewRender::class,
            'viewPath' => FRAMEWORK_PATH . '/Views',
            'cachePath' => BASE_PATH . '/data/cache'
        ],
        'dbConnection' => [
            'class' => DbConnectionManager::class,
            'connections' => [
                'main' => [
                    'username' => 'root',
                    'password' => 'root',
                    'dbname' => 'blog',
                    'host' => 'db',
                    'port' => 3306
                ]
            ]
        ],
        'request' => [
            'class' => Request::class
        ],
        'session' => [
            'class' => Session::class
        ],
        'response' => [
            'class' => Response::class
        ],
        'auth' => [
            'class' => Auth::class,
            'userRepositoryClass' => MysqlUserRepository::class
        ]
    ],
];