<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 7:03 PM
 */

namespace AlexGaj\Blog\Components;


interface ConnectionManagerInterface
{
    public function getConnection(string $connectionName): array;
}