<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 5:25 PM
 */

namespace AlexGaj\Blog\Components;


interface RenderViewInterface
{
    public function render(string $viewName, array $params = []);
}