<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 7:16 PM
 */

namespace AlexGaj\Blog\Components;


class Request implements ComponentInterface
{
    public function init()
    {
        return true;
    }

    public function post($fieldName, $defaultValue = null)
    {
        return $_POST[$fieldName] ?? $defaultValue;
    }

    public function get($fieldName, $defaultValue = null)
    {
        return $_GET[$fieldName] ?? $defaultValue;
    }

    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }
}