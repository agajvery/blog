<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 7:04 PM
 */

namespace AlexGaj\Blog\Components;


class DbConnectionManager implements ConnectionManagerInterface, ComponentInterface
{
    protected $connection;

    public function setConnections(array $connections)
    {
        $this->connection = $connections;
    }

    public function getConnection(string $connectionName): array
    {
        return $this->connection[$connectionName];
    }

    public function init()
    {
        return true;
    }
}