<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 7:22 PM
 */

namespace AlexGaj\Blog\Components;


class Session implements ComponentInterface
{
    public function init()
    {
        session_start();
    }

    public function set($field, $value)
    {
        $_SESSION[$field] = $value;
    }

    public function get($field, $default = null)
    {
        return $_SESSION[$field] ?? $default;
    }
}