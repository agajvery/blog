<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 5:26 PM
 */

namespace AlexGaj\Blog\Components;

class BladeViewRender implements RenderViewInterface, ComponentInterface
{
    private $viewPath;

    private $cachePath;

    public function setViewPath(string $viewPath)
    {
        $this->viewPath = $viewPath;
    }

    public function setCachePath(string $cachePath)
    {
        $this->cachePath = $cachePath;
    }

    public function init()
    {
        return true;
    }

    public function render(string $viewName, array $params = [])
    {
//        $params = array_merge([
//            'isLoginUser' => false,
//        ], $params);

        $blade = new \Jenssegers\Blade\Blade($this->viewPath, $this->cachePath);
        echo $blade->make($viewName, $params)->render();
    }
}