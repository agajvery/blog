<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 5:15 PM
 */

namespace AlexGaj\Blog\Components;


use AlexGaj\Blog\Exceptions\InvalidConfigurationException;


/**
 * Class ServiceLocator
 * @package AlexGaj\Blog\Components
 *
 * @property RenderViewInterface $render
 * @property ConnectionManagerInterface $dbConnection
 * @property Request $request
 * @property Session $session
 * @property Response $response
 * @property Auth $auth
 */
final class ServiceLocator
{
    /**
     * @var ServiceLocator
     */
    private static $instance = null;

    /**
     * @var array
     */
    private $components = [];

    private function __construct(array $components)
    {
        foreach ($components as $name => $config) {
            $this->setComponent($name, $config);
        }
    }

    private function __clone()
    {

    }

    private function __wakeup()
    {

    }

    public function __get($name)
    {
        return $this->getComponent($name);
    }

    public function __set($name, $value)
    {
        $this->setComponent($name, $value);
    }

    protected function setComponent(string $componentName, array $componentConfig)
    {
        if (!isset($componentConfig['class']) || !class_exists($componentConfig['class'])) {
            throw new InvalidConfigurationException(sprintf('Invalid class for component %s', $componentName));
        }

        $componentClass = $componentConfig['class'];
        $component = new $componentClass;

        if (!$component instanceof ComponentInterface) {
            throw new InvalidConfigurationException(sprintf('Component must implement interface %s', ComponentInterface::class));
        }

        unset($componentConfig['class']);

        foreach ($componentConfig as $key => $value) {
            $methodName = 'set' . ucfirst($key);
            $component->{$methodName}($value);
        }

        $component->init();
        $this->components[$componentName] = $component;
    }

    protected function getComponent($name)
    {
        if (isset($this->components[$name])) {
            return $this->components[$name];
        }
        throw new Exception(sprintf('Component %s not found', $name));
    }

    public static function create(array $components)
    {
        if (self::$instance === null) {
            self::$instance = new self($components);
        }

        return self::$instance;
    }
}