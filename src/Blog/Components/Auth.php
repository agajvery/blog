<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 7:45 PM
 */

namespace AlexGaj\Blog\Components;


use Exception;
use AlexGaj\Blog\Models\Entity\User;
use AlexGaj\Blog\Models\Repossitory\User\UserRepositoryInterface;

class Auth implements ComponentInterface
{
    const SESSION_KEY = 'user_id';

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    private $user;

    public function init()
    {
        session_start();

        $userId = $_SESSION[self::SESSION_KEY] ?? null;
        if ($userId !== null) {
            $this->user = $this->userRepository->getById($userId);
        }
    }

    public function setUserRepositoryClass(string $className)
    {
        $this->userRepository = new $className();
        if (!$this->userRepository instanceof UserRepositoryInterface) {
            throw new Exception('Invalid class');
        }
    }

    public function setUserId(int $userId)
    {
        $_SESSION[self::SESSION_KEY] = $userId;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

}