<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 5:22 PM
 */

namespace AlexGaj\Blog\Components;


interface ComponentInterface
{
    public function init();
}