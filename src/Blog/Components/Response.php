<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 7:38 PM
 */

namespace AlexGaj\Blog\Components;


class Response implements ComponentInterface
{

    public function init()
    {
        return true;
    }

    public function redirect(string $url)
    {
        header("Location: $url");
        exit();
    }
}