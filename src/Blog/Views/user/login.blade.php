@extends('layouts.main')

@section('content')
    @if ($errors)
    <div class="errors">
        <div class="col-12">
            {{$errors}}
        </div>

    </div>
    @endif
    <div class="row">
        <form method="post" action="/login">
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input value="{{$email}}" type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

@endsection