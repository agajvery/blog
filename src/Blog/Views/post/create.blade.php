@extends('layouts.main')

@section('content')
    @if ($errors)
        <div class="errors">
            <div class="col-12">
                {{$errors}}
            </div>

        </div>
    @endif
    <div class="row">
        <form method="post" action="/post/add">
            <div class="form-group">
                <label for="exampleInputTitle">Title</label>
                <input value="{{$title}}" type="text" name="title" class="form-control" placeholder="Enter title">
            </div>
            <div class="form-group">
                <label for="exampleInputTitle">Link to the image</label>
                <input value="{{$imageUrl}}" type="text" name="image_url" class="form-control" placeholder="Enter link">
            </div>
            <div class="form-group">
                <label for="exampleInputTitle">Link to the image</label>
                <textarea name="text" class="form-control">{{$text}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>

@endsection