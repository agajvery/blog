<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 8:26 PM
 */

namespace AlexGaj\Blog\Models\Entity;


class Post extends BaseEntity
{
    private $title;

    private $imageUrl;

    private $text;

    private $userId;

    private $createTs;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText(string $text)
    {
        $this->text = $text;
    }

    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    public function setImageUrl(string $imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId(int $userid)
    {
        $this->userId = $userid;
    }

    public function getCreateTs()
    {
        return $this->createTs;
    }

    public function setCreateTs(int $createTs)
    {
        $this->createTs = $createTs;
    }
}