<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 6:32 PM
 */

namespace AlexGaj\Blog\Models\Entity;


abstract class BaseEntity
{
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function __set($name, $value)
    {
        $methodName = 'set' . str_replace('_', '', ucwords($name, '_'));
        call_user_func([$this, $methodName], $value);
    }
}