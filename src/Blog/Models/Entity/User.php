<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 6:31 PM
 */

namespace AlexGaj\Blog\Models\Entity;


class User extends BaseEntity
{
    protected $email;

    protected $password;

    protected $name;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function isValidPassword(string $checkPassword)
    {
        return password_verify($checkPassword, $this->password);
    }

    public function getPasswordHash(string $password)
    {
        password_hash($password, PASSWORD_DEFAULT);
    }
}