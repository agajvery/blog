<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 8:33 PM
 */

namespace AlexGaj\Blog\Models\Factory;


use AlexGaj\Blog\Models\Entity\Post;

class PostEntityFactory
{
    public function newPost(string $title, string $text, string $imageUrl, int $userId): Post
    {
        $postEntity = new Post();

        $postEntity->setUserId($userId);
        $postEntity->setTitle($title);
        $postEntity->setText($text);
        $postEntity->setImageUrl($imageUrl);
        $postEntity->setCreateTs(time());

        return $postEntity;
    }
}