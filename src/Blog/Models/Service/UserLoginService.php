<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 6:55 PM
 */

namespace AlexGaj\Blog\Models\Service;


use AlexGaj\Blog\Models\Entity\User;
use AlexGaj\Blog\Models\Repossitory\User\UserRepositoryInterface;

class UserLoginService
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function login(string $email, string $password): ?User
    {
        $userEntity = $this->userRepository->getUserByEmail($email);
        if ($userEntity && $userEntity->isValidPassword($password)) {
            return $userEntity;
        }
        return null;
    }
}