<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 8:41 PM
 */

namespace AlexGaj\Blog\Models\Service;

use AlexGaj\Blog\Models\Entity\Post;
use AlexGaj\Blog\Models\Repossitory\Post\PostRepositoryInterface;

class PostService
{
    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function createPost(string $title, string $text, string $imageUrl, int $userId): ?Post
    {
        $postEntity = new Post();

        $postEntity->setUserId($userId);
        $postEntity->setTitle($title);
        $postEntity->setText($text);
        $postEntity->setImageUrl($imageUrl);
        $postEntity->setCreateTs(time());

        if ($this->postRepository->save($postEntity)) {
            return $postEntity;
        }

        return null;
    }

    public function getPost(int $postId): Post
    {
        return $this->postRepository->getById($postId);
    }

    public function getLastPosts(int $number, ?int $lastId = null)
    {
        $posts = $this->postRepository->getLastPosts($number, $lastId);
        return $posts;
    }
}