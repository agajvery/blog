<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 6:37 PM
 */

namespace AlexGaj\Blog\Models\Repossitory;


use AlexGaj\Blog\Models\Entity\BaseEntity;

interface RepositoryInterface
{
    public function getById(int $id): ?BaseEntity;

    public function getAll(): array;

    public function save(BaseEntity $entity): bool;

    public function delete(int $id): bool;
}