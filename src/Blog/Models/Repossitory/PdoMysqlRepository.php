<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 6:40 PM
 */

namespace AlexGaj\Blog\Models\Repossitory;


use PDO;
use AlexGaj\Blog\Models\Entity\BaseEntity;

abstract class PdoMysqlRepository
{
    /**
     * @var PDO
     */
    protected $pdo;

    abstract protected function getConnection(): array;

    abstract protected function getTableName(): string;

    abstract protected function getPrimaryKey(): string;

    abstract protected function getEntity(): BaseEntity;

    abstract protected function convertToArray(BaseEntity $entity): array;

    public function __construct()
    {
        $connection = $this->getConnection();

        $dsn = "mysql:dbname={$connection['dbname']};host={$connection['host']};port={$connection['port']}";
        $this->pdo = new PDO($dsn, $connection['username'], $connection['password']);
    }

    protected function createEntity($params): BaseEntity
    {
        $entity = $this->getEntity();
        foreach ($params as $key => $value) {
            $entity->{$key} = $value;
        }
        return $entity;
    }

    public function getAll(): array
    {
        $result = [];

        $query = 'SELECT * FROM ' . $this->getTableName();
        $rows = $this->getRows($query);

        foreach ($rows as $row) {
            $result[] = $this->createEntity($row);
        }

        return $result;
    }

    public function getById(int $id): ?BaseEntity
    {
        $entity = null;

        $query = 'SELECT * FROM ' . $this->getTableName() . ' WHERE ' . $this->getPrimaryKey() . ' = :id';
        $statement = $this->pdo->prepare($query);
        $statement->execute([':id' => $id]);

        $row = $statement->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            $entity = $this->createEntity($row);
        }
        return $entity;
    }

    public function save(BaseEntity $entity): bool
    {
        $params = $this->convertToArray($entity);

        if ($entity->getId() && $this->getById($entity->getId())) {
            return $this->update($entity->getId(), $params);
        } else {
            $id = $this->add($params);
            if ($id) {
                $entity->setId($id);
                return true;
            }
        }
        return false;
    }

    public function delete(int $id): bool
    {
        $query = (
            'DELETE FROM ' . $this->getTableName() .
            ' WHERE ' . $this->getPrimaryKey() . ' = :id'
        );

        $statement = $this->pdo->prepare($query);
        return $statement->execute([':id' => $id]);
    }

    protected function update(int $id, array $params): bool
    {
        $bindParams = [':id' => $id];
        $updateParams = [];

        foreach (array_keys($params) as $field) {
            $key = ':' . $field;
            $updateParams[] = ($field . ' = ' . $key);
            $bindParams[$key] = $params[$field];
        }

        $query = ('
            UPDATE ' . $this->getTableName() . ' 
            SET ' . implode(', ', $updateParams) . '
            WHERE ' . $this->getPrimaryKey() . ' = :id'
        );

        $statement = $this->pdo->prepare($query);
        return $statement->execute($bindParams);

    }

    protected function add(array $params): ?int
    {
        $insertParams = [];
        $bindParams = [];

        $columns = array_keys($params);
        foreach ($columns as $column) {
            $key = ':' . $column;
            $insertParams[] = $key;
            $bindParams[$key] = $params[$column];
        }

        $query = ('
            INSERT INTO ' . $this->getTableName() .
            '(' . implode(', ', $columns) . ') '.
            'VALUES (' . implode(', ', $insertParams) . ')'
        );

        $statement = $this->pdo->prepare($query);
        if ($statement->execute($bindParams)) {
            return $this->pdo->lastInsertId();
        }
        return null;
    }

    protected function findBy(array $params): array
    {
        $where = [];
        $bindParams = [];

        foreach ($params as $column => $value) {
            $key = ':' . $column;
            $where[] = ($column . ' = ' . $key);
            $bindParams[$key] = $value;
        }

        $query = (
            'SELECT * FROM ' . $this->getTableName() .
            ' WHERE ' . implode(' AND ', $where)
        );

        $result = [];
        $rows = $this->getRows($query, $bindParams);

        foreach ($rows as $row) {
            $result[] = $this->createEntity($row);
        }
        return $result;
    }

    protected function getRows(string $sqlQuery, array $params = []): array
    {
        var_dump($sqlQuery);
        $statement = $this->pdo->prepare($sqlQuery);
        $statement->execute($params);
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

        return is_array($rows) ? $rows : [];
    }
}