<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 8:28 PM
 */

namespace AlexGaj\Blog\Models\Repossitory\Post;


use AlexGaj\Blog\Models\Repossitory\RepositoryInterface;

interface PostRepositoryInterface extends RepositoryInterface
{
    public function getLastPosts(int $limit, ?int $lessId): array;
}