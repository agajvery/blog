<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 8:28 PM
 */

namespace AlexGaj\Blog\Models\Repossitory\Post;


use AlexGaj\Blog\Models\Entity\BaseEntity;
use AlexGaj\Blog\Models\Entity\Post;
use AlexGaj\Blog\Models\Repossitory\PdoMysqlRepository;

class MysqlPostRepository extends PdoMysqlRepository implements PostRepositoryInterface
{

    protected function getConnection(): array
    {
        return [
            'username' => 'root',
            'password' => 'root',
            'dbname' => 'blog',
            'host' => 'db',
            'port' => 3306
        ];
    }

    protected function getTableName(): string
    {
        return 'posts';
    }

    protected function getPrimaryKey(): string
    {
        return 'id';
    }

    protected function getEntity(): BaseEntity
    {
        return new Post();
    }

    protected function convertToArray(BaseEntity $entity): array
    {
        return [
            'title' => $entity->getTitle(),
            'image_url' => $entity->getImageUrl(),
            'text' => $entity->getText(),
            'user_id' => $entity->getUserId(),
            'create_ts' => $entity->getCreateTs(),
        ];
    }

    public function getLastPosts(int $limit, ?int $lessId): array
    {
        $params = [];
        $query = 'SELECT * FROM ' . $this->getTableName();

        if ($lessId !== null) {
            $query .= ' WHERE id < :id';
            $params[':id'] = $lessId;
        }

        $query .= (' ORDER BY create_ts LIMIT ' . $limit);

        $result = [];
        $posts = $this->getRows($query, $params);
        foreach ($posts as $post) {
            $result[] = $this->createEntity($post);
        }

        return $result;
    }
}