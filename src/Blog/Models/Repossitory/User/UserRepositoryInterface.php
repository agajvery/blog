<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 6:38 PM
 */

namespace AlexGaj\Blog\Models\Repossitory\User;


use AlexGaj\Blog\Models\Entity\User;
use AlexGaj\Blog\Models\Repossitory\RepositoryInterface;

interface UserRepositoryInterface extends RepositoryInterface
{
    public function getUserByEmail(string $email): ?User;
}