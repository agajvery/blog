<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 6:43 PM
 */

namespace AlexGaj\Blog\Models\Repossitory\User;


use PDO;
use AlexGaj\Blog\Models\Entity\BaseEntity;
use AlexGaj\Blog\Models\Entity\User;
use AlexGaj\Blog\Models\Repossitory\PdoMysqlRepository;

class MysqlUserRepository extends PdoMysqlRepository implements UserRepositoryInterface
{

    protected function getConnection(): array
    {
        return [
            'username' => 'root',
            'password' => 'root',
            'dbname' => 'blog',
            'host' => 'db',
            'port' => 3306
        ];
    }

    protected function getTableName(): string
    {
        return 'users';
    }

    protected function getPrimaryKey(): string
    {
        return 'id';
    }

    protected function getEntity(): BaseEntity
    {
        return new User();
    }

    protected function convertToArray(BaseEntity $entity): array
    {
        return [
            'email' => $entity->getEmail(),
            'password' => $entity->getPassword(),
            'name' => $entity->getName()
        ];
    }

    public function getUserByEmail(string $email): ?User
    {
        $query = 'SELECT * FROM ' . $this->getTableName() . ' WHERE email = :email';
        $statement = $this->pdo->prepare($query);
        $statement->execute([':email' => $email]);

        $userData = $statement->fetch(PDO::FETCH_ASSOC);
        if ($userData) {
            return $this->createEntity($userData);
        }

        return null;
    }
}