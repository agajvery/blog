<?php

use AlexGaj\Blog\App;
use AlexGaj\Blog\Components\ConnectionManagerInterface;
use AlexGaj\Blog\Components\RenderViewInterface;
use AlexGaj\Blog\Components\Session;

/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 5:12 PM
 */

function view(): RenderViewInterface
{
    return App::$components->render;
}

function dbConnection(): ConnectionManagerInterface
{
    return App::$components->dbConnection;
}

function session(): Session
{
    return App::$components->session;
}