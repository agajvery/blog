<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 4:44 PM
 */

namespace AlexGaj\Blog;

use AlexGaj\Blog\Components\ServiceLocator;
use AlexGaj\Blog\Exceptions\InvalidConfigurationException;

require_once FRAMEWORK_PATH . '/helpers.php';

class App
{
    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var ServiceLocator
     */
    public static $components;

    public function __construct(array $configs)
    {
        foreach ($configs as $paramName => $value) {
            $methodName = 'set' . ucfirst($paramName);
            if (method_exists($this, $methodName)) {
                $this->$methodName($value);
            } else {
                throw new InvalidConfigurationException(sprintf('Undefined config param %s', $paramName) );
            }
        }
    }

    public function run()
    {
        if (preg_match('/(.*)\?/', $_SERVER['REQUEST_URI'], $match)) {
            $path = $match[1];
        } else {
            $path = $_SERVER['REQUEST_URI'];
        }
        $controllerParams = $this->getController($path);
        if ($controllerParams === null) {
            header('HTTP/1.0 404 Not Found');
            exit(0);
        }

        $controllerClassName = $controllerParams['controller'] ?? null;
        $actionName = $controllerParams['action'] ?? null;

        $controller = new $controllerClassName();
        $controller->{$actionName}();
    }

    private function setRoutes($routes)
    {
        foreach ($routes as $path => $params) {
            if (!isset($params['controller']) || !isset($params['action'])) {
                throw new InvalidConfigurationException('Controller and action are required');
            }
            $this->routes[$path] = $params;
        }

    }

    private function setComponents(array $components)
    {
        self::$components = ServiceLocator::create($components);
    }

    protected function getController(string $uriPath): ?array
    {
        $controllerParams = null;
        foreach ($this->routes as $key => $params) {
            if ($uriPath == $key) {
                $controllerParams = $params;
                break;
            }
        }

        return $controllerParams;
    }
}