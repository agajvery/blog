<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 5:00 PM
 */

namespace AlexGaj\Blog\Controllers;

use AlexGaj\Blog\Models\Repossitory\Post\MysqlPostRepository;
use AlexGaj\Blog\Models\Service\PostService;

class MainController
{
    public function indexAction()
    {
        $numberOfPosts = 10;

        $postService = new PostService(
            new MysqlPostRepository()
        );
        $posts = $postService->getLastPosts($numberOfPosts);

        view()->render('/main/index', ['posts' => $posts]);
    }
}