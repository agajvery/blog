<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 6:04 PM
 */

namespace AlexGaj\Blog\Controllers;


use AlexGaj\Blog\App;
use AlexGaj\Blog\Models\Repossitory\User\MysqlUserRepository;
use AlexGaj\Blog\Models\Service\UserLoginService;

class UserController
{
    public function loginAction()
    {
        $request = App::$components->request;

        $email = $request->post('email', '');
        $password = $request->post('password', '');
        $errors = [];

        if ($request->isPost()) {
            if ($email && $password) {
                $serviceLoginUser = new UserLoginService(
                    new MysqlUserRepository()
                );

                $user = $serviceLoginUser->login($email, $password);
                if ($user) {
                    App::$components->auth->setUserId($user->getId());
                    App::$components->response->redirect('/');
                } else {
                    $errors[] = 'Incorrect email or password';
                }
            } else {
                $errors[] = 'Email and password are required';
            }
        }

        view()->render('/user/login',[
            'email' => $email,
            'errors' => implode('<br/>', $errors)
        ]);
    }
}