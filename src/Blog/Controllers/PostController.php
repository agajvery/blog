<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 2/3/20
 * Time: 8:20 PM
 */

namespace AlexGaj\Blog\Controllers;


use AlexGaj\Blog\App;
use AlexGaj\Blog\Models\Repossitory\Post\MysqlPostRepository;
use AlexGaj\Blog\Models\Service\PostService;

class PostController
{
    public function addAction()
    {
        $request = App::$components->request;

        $user = App::$components->auth->getUser();
        if ($user === null) {
            App::$components->response->redirect('/login');
        }

        $title = $request->post('title', '');
        $text = $request->post('text', '');
        $imageUrl = $request->post('image_url', '');

        $errors = [];
        if ($request->isPost()) {
            if ($title && $text && $imageUrl) {
                $postService = new PostService(new MysqlPostRepository());
                $post = $postService->createPost(
                    $title,
                    $text,
                    $imageUrl,
                    $user->getId()
                );

                if ($post) {
                    App::$components->response->redirect('/post/view?post_id=' . $post->getId());
                }
            } else {
                $errors[] = 'Title, text and imageUrl are required';
            }
        }

        view()->render('/post/create', [
            'title' => $title,
            'text' => $text,
            'imageUrl' => $imageUrl,
            'isLoginUser' => true,
            'errors' => implode(' ', $errors)
        ]);
    }

    public function viewAction()
    {
        $user = App::$components->auth->getUser();
        if ($user === null) {
            App::$components->response->redirect('/login');
        }

        $postId = App::$components->request->get('post_id', null);
        if ($postId) {

            $postService = new PostService(new MysqlPostRepository());
            $post = $postService->getPost($postId);
            if ($post) {
                view()->render('/post/view', [
                    'title' => $post->getTitle(),
                    'text' => $post->getText(),
                    'imageUrl' => $post->getImageUrl(),
                    'createTs' => date('Y-m-d H:i:s', $post->getCreateTs()),
                    'isLoginUser' => true,
                ]);
            }
        }
    }

    public function myPostsAction()
    {
        $user = App::$components->auth->getUser();
        if ($user === null) {
            App::$components->response->redirect('/login');
        }

        
    }
}